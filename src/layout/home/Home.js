import React from 'react';
import NavBar from '../../components/navigation/NavBar';

function Home() {
  return (
    <>
      <NavBar />
      <h1>HOME</h1>
    </>
  )
}

export default Home;
