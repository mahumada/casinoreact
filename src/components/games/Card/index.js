import React, {useState, useEffect} from 'react';
import { Wrapper, Image } from './styles.js';
import store from '../../../store/reducers/store';
import Blackjack from '../../../layout/blackjack/Blackjack.js';
import flipCard from '../../../store/actionCreators/flipCard.js';

const Card = ({index, imgSrc}) => {
  
  const [isFlipped, setIsFlipped] = useState(true);

  store.subscribe(()=>{
    if(store.getState().cards[index]){
      if(store.getState().cards[index].flipped){
        document.getElementById(`blankSide${index}`).className = 'unflipped';
        setTimeout(()=>{
          document.getElementById(`cardSide${index}`).className = 'flipped';
        }, 500);
        setIsFlipped(!isFlipped);
      }
    }
  })
  
  const style = { 
    paddingLeft: 2*index + 'vw',
    paddingTop: 2*index + 'vw', 
    zIndex: index + 2
  };

  return (
    <Wrapper>
      <Image style={style} id={`blankSide${index}`} className='flipped' src='assets/cartas/card-back.png'/>
      <Image style={style} id={`cardSide${index}`} className='unflipped' src={`assets/cartas/${imgSrc}.png`}/>
    </Wrapper>
  )
}

export default Card;