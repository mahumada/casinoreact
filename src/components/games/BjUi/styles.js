import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 22%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
  position: absolute;
  z-index: 10000;
  flex-wrap: wrap;
  bottom: 5%;
  left: 2%;
  @media(max-width: 650px){
    display: none;
  }
`

export const UiChip = styled.button`
  width: 30%;
  height: 100%;
  z-index: 10001;
  border: none;
  transition: .5s;
  position: relative;
  background: none;
  margin-bottom: 3%;
  &:hover {
    transform: scale(1.12);
    // transform: rotate(360deg);
  }
`

export const UiChipRenderer = styled.img`
  width: 100%;
  height: 100%;
  z-index: 10002;
  padding: 0;
  margin: 0;
  position: relative;
`

export const ChipDelete = styled.button`
  width: 30%;
  font-size: 3rem;
  border-radius: 4rem;
  background: black;
  color: white;
  &:hover {
    transform: scale(1.12);
    // transform: rotate(360deg);
  }
`