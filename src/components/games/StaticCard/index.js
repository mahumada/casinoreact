import React, {useState, useEffect} from 'react';
import { Wrapper, Image } from './styles.js';
import store from '../../../store/reducers/store';
import Blackjack from '../../../layout/blackjack/Blackjack.js';
import flipCard from '../../../store/actionCreators/flipCard.js';

const StaticCard = ({index, imgSrc, isDealer}) => {
  
  const style = isDealer ? { 
    paddingLeft: 2*index + 'vw',
    paddingTop: 2*index + 'vw', 
    zIndex: index + 2,
    top: '2%'
  } : {
    paddingLeft: 2*index + 'vw',
    paddingTop: 2*index + 'vw', 
    zIndex: index + 2
  };



  return (
      <Image style={style} id={`cardSide${index}`} className='unflipped' src={`assets/cartas/${imgSrc}.png`}/>
  )
}

export default StaticCard;