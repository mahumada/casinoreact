import React from 'react';
import { Btn } from './styles';

const HiddenBtn = ({ width, height, top, left, clickFunction }) => {
  return (
    <Btn style={{width, height, top, left}} onClick={clickFunction}>
    </Btn>
  )
}

export default HiddenBtn;