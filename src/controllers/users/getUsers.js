const getUsers = () => {
  return fetch('http://localhost:5000/api/users', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .catch(err => console.error(err));
}

export default getUsers;