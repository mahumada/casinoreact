const startBJ = (userId, bet) => {
  const body = {
    userId,
    bet
  }
  return fetch(`http://localhost:5000/api/blackjack`, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(err => console.error(err))
}

export default startBJ;